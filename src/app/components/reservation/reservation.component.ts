import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ReservationService } from 'src/app/services/reservation.service';
@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {
  reserveForm: FormGroup;
  minDate = Date.now();
  constructor(private reservationService: ReservationService, private fb: FormBuilder, private _snackBar: MatSnackBar) { 
      this.reserveForm = this.fb.group({
      date: ['', Validators.required],
      time: ['', Validators.required],
    })

  }

  ngOnInit(): void {
  }

  getAvaiability() {
    let msg = "Votre ressource est indisponible"
    let date = new Date(this.reserveForm.get('date')?.value);
    let time = new Date(this.reserveForm.get('time')?.value);
    date.setHours(time.getHours());
    date.setMinutes(time.getSeconds());

    this.reservationService.checkAvailability(1337, date).subscribe(res => {
      if (res.available) {
        msg = "Votre ressource est disponible!"
      }
      this._snackBar.open(msg)._dismissAfter(5000);
    });
  }
}
